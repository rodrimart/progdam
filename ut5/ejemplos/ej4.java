import java.io.*;

class ej4 
{
	public static void main(String args[]) throws IOException
	{
		String sentencias = "a=a+1;c++;b+=5;c=a+b;b++;";
		StringReader sr = new StringReader(sentencias);
		PushbackReader pbr = new PushbackReader(sr);
		int ultimo=pbr.read(),penultimo=0;
		while (ultimo != -1)
		{
			if (ultimo == '+')
				// lee el siguiente y comprueba si es otro '+'
				if ((ultimo = pbr.read()) == '+')
						System.out.print("="+(char) penultimo+"+1");
				else
				{
					// si no era otro '+', lo devuelvo
					pbr.unread(ultimo);
					System.out.print('+');
				}
			else
				System.out.print((char)ultimo);
			penultimo=ultimo;
			ultimo = pbr.read();
		}
	}
}

// Investiga qué diferencia existe entre capturar una excepción (try ... catch) y lanzarla (throws)
