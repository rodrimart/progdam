// Programa que permita crear un directorio (similar al mkdir del SO)
import java.io.File;

public class mkdir
{
	public static void main(String[] args)
	{
		File f;
		
		if (args.length > 0 )	// si se pasa al menos un parámetro
		{
			for( int i=0 ; i < args.length ; i++ )
			{
				f = new File(args[i]);
				if (f.mkdir())	// mkdir es boolean: true si lo crea, false si falla
					System.out.println("Directorio creado con éxito");
				else
					System.out.println("Error: no se ha podido crear");
			}
		}
		else
			System.out.println("Forma de uso: java mkdir /ruta/al/directorio");
	}
}
