/* Programa que simule el comando cp (cp origen destino)
 para generar una copia de un fichero */

 import java.io.*;

 public class ejCp
 {
 	public static void main(String[] args) {
 		if (args.length > 1)	// al menos 2 parámetros
 		{
 			int c;
 			try
 			{
	 			FileInputStream fis = new FileInputStream(args[0]);
	 			FileOutputStream fos = new FileOutputStream(args[1],true);

	 			c = fis.read();
	 			while (c != -1)
	 			{
	 				fos.write(c);
	 				c = fis.read();
	 			}
	 			//if (fis != null)
	 				fis.close();
	 			//if (fos != null)
	 				fos.close();
	 		}
	 		/*catch(FileNotFoundException e)
	 		{
	 			System.out.println("El fichero de origen no existe");
	 		}*/
	 		catch(IOException e)
	 		{
	 			System.err.println(e.getMessage());
	 		}
 		}
 		else
 			System.out.println("Forma de uso: java ejCp origen destino");
 	}
 }
