import java.io.*;

public class ejCharArrayWriter
{
	public static void main(String[] args) throws IOException {
		String s = "Esto es un String";
		StringReader sr = new StringReader(s);
		CharArrayWriter caw = new CharArrayWriter();
		int c = sr.read();
		while (c!=-1)
		{
			caw.write(c);
			c = sr.read();
		}
	}

}

		