/* Ejemplo de fichero de acceso aleatorio:

	código que pida pares de números (el primero entero (posición) y el segundo real (valor a escribir)) y escriba el valor en la posición
indicada. El usuario escribirá tantos pares como quiera, hasta acabar con una posición igual a cero. */

import java.io.*;
import java.util.Scanner;

public class ejAleatorio
{
	public static void main(String[] args) {
		int pos; double valor;
		Scanner ent = new Scanner(System.in);
		try (RandomAccessFile raf = new RandomAccessFile("nums.dat","rw");)
		{
			
			System.out.println("Introduce, por este orden, entero (posición) y real (valor a escribir) (posición 0 para acabar):");
			pos = ent.nextInt();
			while (pos != 0)
			{
				valor = ent.nextDouble();
				// primero me posiciono
				raf.seek((pos-1)*8);	// Double.SIZE/8
				// ahora ya escribo el valor en esa posición
				raf.writeDouble(valor);
				System.out.println("Introduce, por este orden, entero (posición) y real (valor a escribir) (posición 0 para acabar):");
				pos = ent.nextInt();
			}
		}
		catch(IOException e)
		{
			System.err.println(e.getMessage());
		}
	}
}