/* 1. Programa que muestre unas determinadas líneas de un
 fichero de texto. Tanto el fichero como los números de línea
  (en orden creciente) serán pasados como parámetros en la
   ejecución (al menos una línea). Ejemplo:

	java ej1 fichero.txt 3 7 10

2. Programa que se ejecute de igual forma que el anterior,
 con un único número de línea, y borre esa línea del fichero
  indicado.

  java ej2 fichero.txt 7 --> borrará del fichero la línea 7
	 */

import java.io.*;

public class ej2
{
	public static void main(String[] args) {
		if (args.length > 1)
		{
			String linea; int cont = 1, i=1;
			File fi = new File(args[0]);
			File fo = new File("tmp.txt");
			try (
				FileReader fr = new FileReader(fi);
				BufferedReader br = new BufferedReader(fr);
				FileWriter fw = new FileWriter(fo);
				)
			{
				linea=br.readLine();
				int numLinea = Integer.parseInt(args[1]);
				while (linea != null)	// for(int i=1; s! null; cont++)
				{
					if (cont != numLinea)
						fw.write(linea);
					linea = br.readLine();
					cont++;
				}
				fi.delete();
				fo.renameTo(fi);
			}
			catch(IOException e)
			{
				System.err.println(e.getMessage());
			}
		}
		else
			System.out.println("Forma de uso: java ej1 /ruta/al/fichero lineaA lineaB lineaC ...");
	}
}