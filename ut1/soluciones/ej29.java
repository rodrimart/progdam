//Programa que determina si el año introducido es bisiesto o no.
public class ej29 
{
    public static void main (String[] args)
    {
        int year;
        
        System.out.println("Introduce un año para saber si es bisiesto");
        year = Integer.parseInt(System.console().readLine());
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
	    	System.out.println("el año " + year + " es bisiesto");
        else
            System.out.println("el año " + year + " no es bisiesto");
    }
}
