/*Realizar un programa que lea pares de números y escriba todos los números que están entre los números de cada par (sin incluirlos) 
y que NO sean ni el cuadrado ni el cubo de números entre el 1 y el 100.
Se terminará el programa cuando alguno de los dos números leídos sea cero o negativo.
Si no hay ningún número que cumpla la condición entre los dados se escribirá un 0.

Ejemplo:
	Números leídos: 3 10,   48 50,    30 25,      0 8
    Resultado:     5 6 7,    0,     26 28 29,   fin*/

import java.util.Scanner;
public class ej33
{
    public static void main(String[]args)
    {
        Scanner sc = new Scanner(System.in);
        boolean shouldExit = false;
        while (!shouldExit)
        {
            System.out.println("Introduce un número:");
            int desde = sc.nextInt();
            System.out.println("Introduce otro número:");
            int hasta = sc.nextInt();
            if (desde <= 0 || hasta <= 0)
            {
                System.out.println("Fin");
                shouldExit = true;
            }
            else
            {
                System.out.println("Resultado:");
                if (hasta < desde)
                {
                    int aux = hasta;
                    hasta = desde;
                    desde = aux;
                }
                for (int i = desde + 1 ; i < hasta ; i++)
                {
                    if (isSquareOfFirst100(i) == false && isCubeOfFirst100(i) == false) //Si el número no es cuadrado y el número no es cubo.
                    {
                        System.out.println(i);
                    }
                }
            }
        }
        sc.close();
    }
    public static boolean isSquareOfFirst100(int inconmigNumber)
    {
        for (int i = 1; i <= 100; i++) //si entre los 100 primeros numeros existe uno que cuyo cuadrado es igual al numero recibido = true
        {
            int squaredNumber = i * i;
            if (squaredNumber == inconmigNumber)
            {
                return true;
            }
            if (squaredNumber > inconmigNumber)
            {
                return false;
            }
        }
        return false;
    }
    public static boolean isCubeOfFirst100(int inconmigNumber)
    {
        for (int i = 1; i <= 100; i++) //si entre los 100 primeros numeros existe uno que cuyo cubo es igual al numero recibido = true
        {
            int cubedNumber = i * i * i;
            if (cubedNumber == inconmigNumber)
            {
                return true;
            }
            if (cubedNumber > inconmigNumber)
            {
                return false;
            }
        }
        return false;
    }
}
