// 22. Algoritmo que solicita notas introducidas por teclado acabadas con un numero negativo, e imprime en pantalla el aprobado de nota más baja y el suspenso de nota más alta.

public class ej22
{

    public static void main(String args[])      
    {
    
        double nota,minApr,maxSusp;

        minApr=11;maxSusp=-1;

        do
        {       
            System.out.println("Introduza nota entre 0 y 10: ");
            nota = Double.parseDouble(System.console().readLine());
            
            if ((nota>=0) && (nota<5))	// suspenso
                if (nota>maxSusp)
                    maxSusp=nota;
        
            if ((nota>=5) && (nota<=10))
                if (nota<minApr)
                    minApr=nota;

        }
        while (nota >=0);
    
        if (minApr<11)
            System.out.println("\nEl aprobado de nota mas baja es " + minApr);
        else
            System.out.println("\nNo ha habido ningun aprobado" );
                           
        if (maxSusp>-1)
            System.out.println("\nEl suspendido de nota mas alta es " + maxSusp);
            else
                System.out.println("\nNo ha habido ningun suspenso");


    System.exit(0);
    }
}

