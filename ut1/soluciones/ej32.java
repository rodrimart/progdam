/* 32. Realizar un programa que lea un número del teclado y escriba en pantalla, en forma de cuadrado, la lista de números naturales desde el 1 hasta el número dado, rellenando los espacios sobrantes con 0.
	Ejemplo:
		Para el número 4:
				1 2 3 4
				2 3 4 0
				3 4 0 0
				4 0 0 0 */
import java.util.Scanner;
			
public class ej32
{
	public static void main(String[] args) {
		int num;
		
		Scanner ent = new Scanner(System.in);
		System.out.println("Introduce un entero:");
		num = ent.nextInt();
		for ( int f = 1 ; f <= num ; f++)
		{
			for ( int c = 1 ; c <= num ; c++)
				if ( (f+c-1) > num)
					System.out.print('0' + "\t");
				else
					System.out.print((f + c -1) + "\t");
			System.out.println("");
		}	
			
				
		System.exit(0);
	}
}
