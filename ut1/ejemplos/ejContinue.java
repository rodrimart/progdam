// Ejemplo de CONTINUE
// Programa que pida 5 notas válidas y muestre su promedio (si alguna de las notas no es válida la ignorará)

public class ejContinue
{
	public final static int N = 5;
	
	public static void main(String[] args)
	{
		double nota,suma=0; int notasValidas=0;
		
		for ( int i = 0 ; i < N ; i++)
		{
			System.out.println("Introduce nota (entre 0 y 10):");
			nota = Double.parseDouble(System.console().readLine());
			// Si la nota no es válida la ignoro con el continue
			if ((nota < 0) || (nota > 10))
				continue;
			notasValidas++;
			suma = suma + nota;	// suma += nota
		}
		if (notasValidas > 0)
			System.out.println("El promedio de las notas es " + suma/notasValidas);
		else
			System.out.println("No ha habido ninguna nota válida");
			
			
		
		System.exit(0);
	}
}
