Ejercicios de algoritmia para resolver en el lenguaje estudiado (C, Java, Python ...) (o bien, pseudocódigo u ordinograma).



1. Programa que lee desde teclado un entero y lo muestra en pantalla (hace un eco).

2. Programa que pide 2 valores numéricos y muestra el resultado de su suma y su producto.

3. Programa que acepta un número y muestra sus valores mitad y doble.

4. Programa que pide un valor numérico e indica si éste es positivo o negativo.

5. Programa que, a partir de un entero introducido desde teclado, muestra todos los enteros, desde 1, menores que él.

6. Programa que acepta 2 números y, si su suma es inferior a 10, pide un tercer valor. Al final muestra la suma de los valores introducidos.

7. Programa que imprime todos los enteros entre 1 y 100.

8. Programa que imprime todos los pares entre 1 y 100.

9. Programa que acepta 3 números y los muestra ordenados.

10*. Programa que muestra las 10 tablas de multiplicar (para 1,2,3,...,10).

11*. Programa que pide un número entero e imprime su tabla de multiplicar.

12*. Programa que pide un número mayor que 0 al usuario y realiza comprobación del valor de manera que si el valor introducido no es correcto vuelva a solicitar la introducción del valor.

13. Realizar el ordinograma y pseudocódigo que lea una cantidad positiva/ negativa de ºC y la pase a grados Farenheit.

14. Realizar el ordinograma y pseudocodigo de un programa que intercambie el valor de dos variables.

15. Realizar la siguiente traza:

INICIO
	Leer a
	c = 0
	Mientras c< 10
		c= c+2
		a= a +3
		Escribir a
	FinMientras
FIN

Datos de entrada: 5

16. Programa que indique el máximo y el mínimo de un conjunto de notas introducidas desde teclado. La serie de notas acabará con un valor negativo.

17.  Sustituir la siguiente estructura "para" por una de tipo "mientras":

	x=1
	y=1
	Para i desde 5 hasta 100 con incremento 5
		z=x+y
		x=y
		y=z
	Fpara
	
18. Diseñar un programa que muestre la suma de los números impares comprendidos entre dos valores numéricos enteros y positivos introducidos por teclado.

19. Programa que acepte números positivos o iguales a 0 hasta acabar con un negativo y muestre por pantalla el total de valores válidos introducidos.

20. Plasmar el siguiente algoritmo en pseudocodigo:

	INICIO
		Escribir "Introduce dos números:"
		Leer n1,n2

		N
	      n2<> 0
	   	S
		Escribir n1*n2
	FIN
		
21. Programa que acepta números enteros mayores o iguales que 0 hasta acabar con un numero negativo e indique cuantos múltiplos de 2 se han introducido, cuantos  múltiplos de 3 y cuantos múltiplos de 2 y de 3 (de 6).

22. Algoritmo que solicita notas introducidas por teclado acabadas con un numero negativo, e imprime en pantalla el aprobado de nota más baja y el suspenso de nota más alta.

23*. Algoritmo que permite calcular el factorial de un numero natural.

24. Algoritmo que lea números desde teclado y finalice cuando se introduzca uno mayor que la suma de los dos anteriores. Escribir en pantalla el numero de valores introducidos y los valores que cumplieron la condición de finalización.

25. Escribir un programa que lea las temperaturas obtenidas en 15 observatorios meteorológicos y escriba la temperatura mínima y cuantas mínimas se han producido.

26. Algoritmo que indique si un numero introducido por teclado es primo.

27*. Modificar el anterior algoritmo para que pida una lista de enteros acabada con un 0  e indique, para cada uno de ellos, si es primo o no. Además, al final ha de indicar cuántos primos se han introducido.

28*. Los números de Fibonacci son los miembros de una secuencia en la que cada numero es igual a la suma de los dos anteriores. En otras palabras, Fi = Fi-1 + Fi-2, donde Fi es el i-ésimo nº de Fibonacci, y siendo F1=F2=1. Por tanto:
	F5 = F4 + F3 = 3 + 2 = 5, y así sucesivamente.
	Escribir un programa que determine los n primeros números de Fibonacci, siendo n un valor introducido por teclado.
	
29.  Algoritmo que indique si un año, introducido por teclado, es bisiesto o no.

30. Realizar un programa que lea enteros hasta que se dé un 0 y calcule la suma de sus dígitos.

31. Realizar un programa que lea la fecha de nacimiento de una persona y la fecha del día actual y escriba la edad en años de la persona.
	Cada fecha estará compuesta por 3 números que indicarán día, mes y año.

32. Realizar un programa que lea un número del teclado y escriba en pantalla, en forma de cuadrado, la lista de números naturales desde el 1 hasta el número dado, rellenando los espacios sobrantes con 0.
	Ejemplo:
		Para el número 4:
				1 2 3 4
				2 3 4 0
				3 4 0 0
				4 0 0 0

33*. Realizar un programa que lea pares de números y escriba todos los números que están entre los números de cada par (sin incluirlos) y que NO sean ni el cuadrado ni el cubo de números entre el 1 y el 100.
	Se terminará el programa cuando alguno de los dos números leídos sea cero o negativo.
	Si no hay ningún número que cumpla la condición entre los dados se escribirá un 0.

	Ejemplo:
		Números leídos: 3 10, 48 50, 30 25, 0 8
		Resultado: 5 6 7, 0, 26 28 29, fin
				
