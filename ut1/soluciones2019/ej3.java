// Programa que acepta un número y muestra sus valores mitad y doble.

import java.util.Scanner;

public class ej3
{
    public static void main(String args[])
    {
        Scanner ent = new Scanner(System.in); // es el teclado
        double num; // variable num

        System.out.println("Introduce un número: ");
        num = ent.nextDouble();

        System.out.println("La mitad de " + num + " es " + (num/2) + " y su doble es " + (num*2));
    }
}
