// 1. Programa que lee desde teclado un entero y lo muestra en pantalla (hace un eco).

public class ej1
{
	public static void main(String args[])
	{
		int num;

		System.out.println("Introduce un entero:");
		num = Integer.parseInt(System.console().readLine());
		// Hago el eco
		System.out.println(num);	
	}
}
