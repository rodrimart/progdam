// 11. Programa que pide un número entero e imprime su tabla de multiplicar.

import java.util.Scanner;

public class ej11
{
	public static void main(String[] args) 
	{
		int num, cont;
		Scanner ent = new Scanner(System.in);
		System.out.println("Dime un número entero y te mostraré su tabla de multiplicar");
		num = ent.nextInt();

		for ( cont=1 ; cont <= 10 ; cont++)
			System.out.println(num + "*" + cont + "=" + num*cont);
	}
}
