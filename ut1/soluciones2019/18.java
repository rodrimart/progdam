// Diseñar un programa que muestre la suma de los números impares comprendidos entre dos valores numéricos enteros y positivos introducidos por teclado.
public class p9
{
	public static void main (String args[])
	{
		int num1, num2, aux;
		int suma = 0;
		int i ;

		System.out.println("Introduce el primer numero:");
		num1 = Integer.parseInt(System.console().readLine());
		System.out.println("Introduce el segundo numero:");
		num2 = Integer.parseInt(System.console().readLine());

		if (num1 > num2)
		{
			aux = num1;
			num1 = num2;
			num2 = aux;
		}

		if (num1 % 2 == 0)
			num1++;
		else
			num1 = num1 + 2;

		for (i = num1; i < num2; i = i+2)
/*		{
			System.out.println("a");*/
			suma = suma + i;
		//}

		System.out.println("La suma de los numeros impares comprendidos entre los introducidos es " + suma);
	}
}
