// 18. Diseñar un programa que muestre la suma de los números impares comprendidos entre dos valores numéricos enteros y positivos introducidos por teclado.

import java.util.Scanner;

public class ej18
{
    public static void main(String args[])
    {
        int num1 = 0, num2 = 0, aux = 0, suma = 0;  
        Scanner ent = new Scanner(System.in);
        
        do 
        {
            System.out.println("Introduce dos enteros positivos diferentes:");
            num1 = ent.nextInt(); 
            num2 = ent.nextInt();
        } while ( (num1<=0) || (num2<=0)); // Realizará la pregunta hasta que los enteros introducidos sean diferentes.

        if ( num1 > num2 )  // si el primero es mayor, los intercambiamos
            {
                aux = num1;
                num1 = num2;
                num2 = aux; 
            }
            
        if ((num1 % 2) == 1 ) // si es impar (le he de sumar 2)
             num1++;
        // else ... es par (sumaría 1)
        for ( int cont = num1 + 1; cont < num2; cont += 2 )
                 suma = suma + cont;    // suma += cont
               
        System.out.println("La suma de los números impares comprendidos entre los valores introducidos es:" + suma);   
    }

}
