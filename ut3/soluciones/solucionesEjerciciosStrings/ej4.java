/* 	4. Implemente un programa que reciba una cadena S y una letra X, y coloque en mayúsculas cada ocurrencia de X en S. (la función debe modificar la variable S). 
*/

public class ej4{
	public static void main(String[] args){
		if (args.length >= 1)
		{
			char c=args[0].charAt(0);
			// comprueba que la letra sea minúscula
			if ((c >= 'a') && (c <= 'z'))
			{
				System.out.println("Introduce un texto:");
				//char d=c
				System.out.println(System.console().readLine().replace(c,(char)(c-32)));
			}

		}
		else
			System.out.println("Forma de uso: ej4 letra" );		
	}
}
