// 2. Escribir un programa que reciba como datos una cadena de caracteres y un carácter y reporta el número de veces que se encuentra el carácter en la cadena. 
import java.util.Scanner;

public class ej2
{
	public static void main(String[] args)
	{
		Scanner ent = new Scanner(System.in);
		int cont = 0,i=0;

		System.out.println("Introduce un texto:");
		String s = ent.nextLine();
		System.out.println("Introduce el caracter a buscar:");
		String carac = ent.nextLine();
		char c = carac.charAt(0);

		// Empezamos a buscar y contar cuántas veces aparece c en el texto s
		while ( i < s.length() )
		{
			if ( c == s.charAt(i) )
				cont++;
			i++;
		}
		System.out.println("El caracter " + c + " ha aparecido " + cont + " veces");
		
		// SEGUNDA FORMA: generando un char [] con el contenido del String
		char txt[] = s.toCharArray();
		i=0; cont = 0;
		while ( i < txt.length )
		{
			if ( c == txt[i] )
				cont++;
			i++;
		}
		System.out.println("El caracter " + c + " ha aparecido " + cont + " veces");
	}
}
