/*
  2. Crea un programa de gestión de discos de música. Para ello, define una clase Disco (o reutiliza la creada en la unidad 2),
  sabiendo que, para cada disco, queremos registrar el título, grupo o artista y el precio.
  Añade a la clase todos los constructores y métodos que consideres útiles o necesarios.

  El programa debe crear un array para 10 discos, inicialmente vacío, y mostrar el siguiente menú:

	1. Alta de disco
	2. Mostrar todos los discos
	3. Modifificar disco existente
	4. Ordenar por precio
	5. Ordenar por título
	0. Salir 
*/

import java.util.Scanner;

class Disc{


	//Atributos
	private String title;
	private String artist;
	private double price;


	/* Constructores */

	//Constructor por defecto
	public Disc(){
		this.title = "Unkown";
		this.artist = "Unkown";
		this.price = 25;
	}

	//Constructor general
	public Disc(String title, String artist, double price){
		this.title = title;
		this.artist = artist;
		this.price = price;
	}

	//Constructor de copia
	public Disc(Disc d){
		this.title = d.title;
		this.artist = d.artist;
		this.price = d.price;
	}


	//Métodos 'setter'
	public void setTitle(String title){ this.title = title; }
	public void setArtist(String artist){ this.artist = artist; }
	public void setPrice(double price){ this.price = price; }


	//Métodos 'getter'
	public String getTitle(){ return this.title; }
	public String getArtist(){ return this.artist; }
	public double getPrice(){ return this.price; }


	/* Métodos adicionales */

	//Método 'toString' para imprimir en pantalla lo datos de un objeto 'Disc'.
	public String toString(){ return ("Título: " + this.title + "\nArtista: " + this.artist + "\nPrecio: " + this.price); }	
}



public class arrays_2{
	
	public static void main(String args[]){


		Scanner sc = new Scanner(System.in);
		// SUSTITUIR 10 POR UNA CONSTANTE
		Disc d[] = new Disc[50];
		boolean exit = false;
		String title, artist;
		double price;
		int choice;
		ProcessBuilder pb = new ProcessBuilder("clear");
		Process startProcess;


		while(!exit){


			System.out.println("\nPROGRAMA DE GESTIÓN DE DISCOS DE MÚSICA\n");
			System.out.println("  * * * * * * * * MENÚ * * * * * * * *");
			System.out.println("  *                                  *");
			System.out.println("  * 1. Dar de alta disco.            *");
			System.out.println("  * 2. Mostrar catálogo.             *");
			System.out.println("  * 3. Modificar ficha de disco.     *");
			System.out.println("  * 4. Ordenar catálogo por precio.  *");
			System.out.println("  * 5. Ordenar catálogo por título.  *");
			System.out.println("  * 0. Salir.                        *");
			System.out.println("  *                                  *");
			System.out.println("  * * * * * * * * * * * * *  * * * * *");

			System.out.print("\nElija una opción del menú superior (1,2,3,4,5,0): ");
			choice = sc.nextInt();
			
			while(choice < 0 || choice >= 6){
				System.out.println("\nError => '" + choice + "' no es una entrada válida.");
				System.out.print("\nElija una opción del menú superior (1,2,3,4,5,0): ");
				choice = sc.nextInt();
			}


			switch(choice){
				//Opción 1: dar de alta un nuevo disco.
				case 1: 

					if(d[9] != null){
						System.out.println("\nEl catálogo de discos está lleno. No es posible almacenar más discos.");
						break;
					}

					System.out.print("\n\nREGISTRO DE NUEVO DISCO\n\n");

					System.out.print("Introduzca el título del disco: ");
					title = System.console().readLine();

					System.out.print("Introduzca el nombre del artista: ");
					artist = System.console().readLine();

					System.out.print("Indique el precio del disco: ");
					price = sc.nextDouble();

					d[discsAmnt(d)] = new Disc(title,artist,price);

					System.out.print("\nNuevo disco registrado con éxito.\n");

					break;


				//Opción 2: mostrar el catálogo de discos.
				case 2:

					if(areThereDiscs(d)){

						System.out.println("\n\nCATÁLOGO DE DISCOS");

						for(int i=0; i<discsAmnt(d); i++)							
							System.out.println("\n" + d[i]);
					}

					break;


				//Opción 3: modificar datos de registro de un disco.
				case 3:

					if(areThereDiscs(d)){

						int i = 0;
						String dscsStrd = "(0";

						System.out.println("\n\nCATÁLOGO DE DISCOS");

						for( ; i<discsAmnt(d); i++){
							dscsStrd = dscsStrd.concat("," + (i+1));						
							System.out.printf("\nDISCO %d\n",(i+1));
							System.out.println(d[i]);
						}
						dscsStrd = dscsStrd.concat(")");

						System.out.print("\nSeleccione el número del disco cuyos datos desea modificar o 0 para volver al menú " + dscsStrd + ": ");
						choice = sc.nextInt();

						while(choice < 0 || choice > i){
							System.out.printf("\nError => No hay ningún disco almacenado con el número %d.\n",choice);
							System.out.print("\nSeleccione el número del disco cuyos datos desea modificar o 0 para volver al menú " + dscsStrd + ": ");
							choice = sc.nextInt();
						}

						if(choice == 0)
							break;

						System.out.print("\n\nMODIFICACIÓN DE DATOS DE REGISTRO\n\n");

						System.out.printf("DISCO %d\n",choice);

						System.out.print("Introduzca el título del disco: ");
						d[choice-1].setTitle(System.console().readLine());

						System.out.print("Introduzca el nombre del artista: ");
						d[choice-1].setArtist(System.console().readLine());

						System.out.print("Indique el precio del disco: ");
						d[choice-1].setPrice(sc.nextDouble());

						System.out.printf("\nDatos de registro del disco %d actualizados con éxito.\n",choice);
					}

					break;


				//Opción 4: ordenar discos por precio.
				case 4:

					if(areThereDiscs(d)){

						boolean sorted = false;
						Disc aux;
						int i = discsAmnt(d)-2;

						System.out.println("\n\nCATÁLOGO DE DISCOS ORDENADO POR PRECIO\n");

						if(d[1] != null)	

							//Algoritmo de la burbuja optimizado.
							for( ; i>=0 && !sorted; i--){

								sorted = true;

								for(int j=0; j<=i; j++)
									if(d[j+1].getPrice() > d[j].getPrice()){
										sorted = false;
										aux = d[j+1];
										d[j+1] = d[j];
										d[j] = aux;
									}
							}

						//Salida por pantalla del catálogo de discos ordenados por precio.
						for(i=0; i<discsAmnt(d); i++)
							System.out.println("\n" + d[i]);
					}

					break;


				//Opción 5: ordenar discos por título.
				case 5:

					if(areThereDiscs(d)){

						boolean sorted = false;
						int i = discsAmnt(d)-2;
						Disc aux;

						System.out.println("\n\nCATÁLOGO DE DISCOS ORDENADO ALFABÉTICAMENTE POR EL TÍTULO\n");

						if(d[1] != null){

							//Algoritmo de la burbuja optimizado.
							for( ; i>=0 && !sorted; i--){

								sorted = true;

								for(int j=0; j<=i; j++)
									if(d[j].getTitle().compareToIgnoreCase(d[j+1].getTitle()) > 0){
										sorted = false;
										aux = d[j+1];
										d[j+1] = d[j];
										d[j] = aux;
									}
							}
						}

						//Salida en pantalla del catálogo de discos ordenado por título alfabéticamente.
						for(i=0; i<discsAmnt(d); i++)
							System.out.println("\n" + d[i]);
					}

					break;

				//Opción 0: abandonar el programa.
				case 0: exit = true;
			}
			
			if(!exit){

				if(choice != 0){

					System.out.print("\n¿Desea realizar alguna otra operación? [s/n] ");
					choice = (int)(sc.next().charAt(0));

					while(choice != 83 && choice != 115 && choice != 78 && choice != 110){
						System.out.print("\nError => '" + (char)choice + "' no es una entrada válida. ¿Desea llevar a cabo alguna otra operación? [s/n] ");
						choice = (int)(sc.next().charAt(0));
					}
				}

				if(choice == 0 || choice == 83 || choice == 115)
					try{
						startProcess = pb.inheritIO().start();
						startProcess.waitFor();
					} catch(Exception e){
						System.out.println(e);
					}
				else
					exit = true;
			}
		}
		System.out.println("\n");
	}


	//Función para conocer si hay discos en el catálogo.
	public static boolean areThereDiscs(Disc d[]){
		
		if(d[0] == null){
			System.out.println("\nEl catálogo de discos está vacío.");
			return false;
		}

		return true;
	}


	//Función para averiguar cuántos discos tiene el catálogo.
	public static int discsAmnt(Disc d[]){

		int i = 0;

		for( ; i<d.length && d[i]!=null; i++);

		return i;
	}
}
