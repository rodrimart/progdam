public class ej1
{
	private static int[]	lista;
	final static int POS = 50;	// número de posiciones del array
	final static int LIMITE	= 100;	// Números entre 1..Límite

	public static int getaleatorio()
	{
		return (int) (Math.random() * LIMITE + 1);
	}

	public static void ordena(int array[])
	{
		int aux;
		boolean cambio;

		for (int i = array.length; i > 0; i--)
		{
			cambio = false;

			for (int j = 0; j < i - 1; j++)
			{
				if (array[j] > array[j + 1])
				{
					aux = array[j + 1];
					array[j + 1] = array[j];
					array[j] = aux;
					cambio = true;
				}
			}
			if (!cambio)
				return;
		}
	}

	public static void muestra()
	{
		for (int i = 0; i < POS; i++)
			System.out.print(lista[i] + " ");
	}

	public static void main(String[] args)
	{
		lista = new int[POS];

		for (int i = 0; i < POS; i++)
			lista[i] = getaleatorio();
		muestra();// se muestra el vector desordenado
		System.out.println("");
		ordena(lista); // ordenación por burbuja
		System.out.println("");
		muestra();// se muestra el vector ordenado
		System.out.println("");
	}
}
