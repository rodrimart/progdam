/* Realiza un programa que cree un vector de 100 posiciones con números aleatorios entre 1 y 100. Una vez creado el vector, 
el programa deberá ordenar el vector y mostrar los números entre 1 y 100 que no han sido almacenados.*/
import java.util.Vector;
public class ejercicio10libro{

	private final static int TAM = 100;

	public static void main(String[] args) {

		int[] numeros = new int[TAM];

		llena(numeros);
		muestra(numeros);
		ordena(numeros);
		System.out.println("\nNúmeros aleatorios del 1 al 100:\n");
		muestra(numeros);
		System.out.println("\nFaltan estos números:\n");
		noEstan(numeros);
	}

	public static /*int[]*/void llena(int[] vector){

		for (int i = 0 ; i < vector.length ; i++) 
		//{
			vector[i] = (int)(TAM * Math.random() + 1);
			//System.out.println(vector[i]);
		//}	
		//return vector;
	}

	public static /*int[]*/ void ordena(int[] vector){

		boolean ordenado = false; int aux;

		for (int tope = vector.length - 1 ; ((tope >= 0) && (!ordenado)) ; tope--) {
			ordenado = true;
			for (int i = 0 ; i < tope ; i++) {
				if(vector[i] > vector[i+1]){
					ordenado = false;
					aux = vector[i];
					vector[i] = vector[i+1];
					vector[i+1] = aux;
				}
			}
		}
		//return vector;
	}

	public static void muestra(int[] nums){
		for (int i = 0 ; i < nums.length ; i++) 
			System.out.printf("[%d] ",nums[i]);
		System.out.println();
	}

	public static void noEstan(int[] nums){

		Vector<Integer> ausentes = new Vector<Integer>();
		boolean siEsta = false;

		for (int num = 1 ; num <= 100 ; num++){
			siEsta = false;
			for (int i = 0 ; i < nums.length ; i++) {
				if(num == nums[i]){
					siEsta = true; break;
				}
			}
			if(!siEsta)
				ausentes.add(num);
		}
		
		for (Integer n:ausentes)
			System.out.printf("[%d] ",n);
		System.out.println();
	}
}
