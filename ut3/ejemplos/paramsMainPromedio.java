// Ejemplo de uso de parámetros a la función main
// Muestra el promedio de los 2 números pasados como parámetros

public class paramsMainPromedio
{
	
	public static void main(String[] args)
	{
		double suma=0;
		
		if (args.length > 1)
		{
			for( int i=0 ; i < 2 ; i++)
				suma += Double.parseDouble(args[i]);
			
			System.out.println("El promedio ha sido " + suma/2 );
		}
		else
			System.out.println("Forma de uso: java programa num1 num2");
	
			System.exit(0);
	}

}
