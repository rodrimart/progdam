// Ejemplo de BÚSQUEDA DICOTÓMICA

import java.util.Scanner;

public class busqDicotomica
{
	private final static int TAM = 4;
	public static void main(String args[])
	{
		Scanner ent = new Scanner(System.in);
		// declaro el array de 5 valores numéricos double
		double aux; /*int comps=0;*/
		double nums[] = new double[TAM];
		// guardo en él los primeros múltiplos de 3
		for (int i = 0 ; i < TAM ; i++)
			//nums[i] = (int)(10*Math.random());
			nums[i] = ent.nextDouble();
		// muestro el contenido del array en pantalla
		System.out.println("El contenido del array es");
		for (int i = 0 ; i < TAM ; i++)
			System.out.println(nums[i]);
		// ORDENO EL ARRAY
		for(int tope = TAM -2 ; tope >= 0 ; tope--)
		{
			System.out.println("Empezando desde la primera posición ...");
			for(int i=0 ; i <= tope ; i++)
				// si es mayor que su siguiente, invierto ambos valores
				if (nums[i] > nums[i+1])
				{
					aux = nums[i];
					nums[i] = nums[i+1];
					nums[i+1] = aux;
				}
		}
		// muestro el contenido del array en pantalla YA ORDENADO
		System.out.println("El contenido del array es");
		for (int i = 0 ; i < TAM ; i++)
			System.out.println(nums[i]);
		//System.out.println("Hemos ordenado utilizando " + comps + " comparaciones.");
		
		// Ahora busco un valor en el array ORDENADO con la búsqueda DICOTÓMICA
		System.out.println("Introduce un valor a buscar:");
		aux = ent.nextDouble();
		if (busquedaDicotomica(nums,aux))
			System.out.println("SI se encuentra en el array");
		else
			System.out.println("NO se encuentra en el array");
		
	}
	
	public static boolean busquedaDicotomica(double n[],double vb)
	{
		int izq,der,centro;
		izq=0;der=n.length-1; centro = (izq+der)/2;
		while ( vb != n[centro] && izq <= der)
		{
			if (vb < n[centro])
				der = centro - 1;
			else
				izq = centro + 1;
			centro =  (izq+der)/2;
		}
		if (vb == n[centro])
			return true;
		else	
			return false;
	}
}
