abstract class Figura
{
	protected String color;
	public Figura(String color)
	{
		this.color = color;
	}
	public abstract double calcularArea();
	public /*final*/ String getColor()
	{
		return color;
	}
}

class Cuadrado extends Figura
{
	private double lado;
	public Cuadrado(String color, double lado)
	{
		super(color);
		this.lado = lado;
	}
	public double calcularArea()
	{
		return lado * lado;
	}
/*	public String getColor()
	{
		return "color: " + color;
	}*/
}

public class ClaseAbstracta
{
	public static void main(String[] args) {
		//Figura f = new Figura("azul");
		Cuadrado c = new Cuadrado("azul", 5);
		System.out.println("El color del cuadrado es " + c.getColor());
		System.exit(0);
	}
}
