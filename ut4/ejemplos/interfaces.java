interface Marcador
{
	void anotaGol();
}

// ESTA CLASE IMPLEMENTARÁ LA INTERFAZ ANTERIOR Y TAMBIÉN CLONEABLE
class Futbolista implements Cloneable, Marcador
{
	// atributo estático
	private static int numFutbolistas;
	private String nombre;
	private int goles;

	// INICIALIZADORES ESTÁTICOS
	static
	{
		numFutbolistas=0;
		System.out.println("Inicializando la clase Futbolista");
	}
	static
	{	
		System.out.println("La clase Futbolista se está inicializando");
	}	
		
	public Futbolista(String n,int g){this.nombre=n; this.goles=g;numFutbolistas++;}
	// CONSTRUCTOR DE COPIA
	public Futbolista(Futbolista f)
	{
		this.nombre=f.nombre;
		this.goles=f.goles;
		System.out.println("He creado "+ getNumFutbolistas()+" futbolistas");numFutbolistas++;
	}	
	public Futbolista(){this.nombre="Iniesta";this.goles=10;numFutbolistas++;}

	// ESTE ES EL MÉTODO AL QUE LA INTERFAZ OBLIGA A DEFINIR
	public void anotaGol(){goles++;}
	public void setNombre(String n){nombre=n;}
	public void setGoles(int g){goles=g;}
	public String getNombre(){return nombre;}
	public int getGoles(){return this.goles;}
	public Futbolista obtieneFutbolista()
	{ return this; }
	public int getNumFutbolistas() { return numFutbolistas; }
	public Object clone()
	{
		Object objeto=null;
		try 
		{
		objeto=super.clone();
		numFutbolistas++;
		}
		catch (CloneNotSupportedException e)
		{
			//e.getMessage();
			System.out.println("Error al clonar");
				
			
		}
		return objeto;
	}
	// EJEMPLO DE MÉTODO STATIC
	public static void setNumFutbolistas(int num)
	{
		numFutbolistas=num;
	}
}

class inicializadores
{
	public static void main(String[] args) {
		Futbolista f1=new Futbolista();
		Futbolista f2=(Futbolista)f1.clone();
		System.out.println("El futbolista 2 se llama: "+f2.getNombre()+" y ha marcado: "+f2.getGoles()+" goles.");
		if (f1.equals(f2)==true)
		//if (f1==f2)
			System.out.println("Son el mismo objeto");
		else
			System.out.println("Son objetos diferentes");
		System.out.println(f1.toString());
		System.out.println(f2.toString());
		System.out.println("He creado "+f2.getNumFutbolistas()+" futbolistas");
		// LLAMADA AL MÉTODO ESTÁTICO
		Futbolista.setNumFutbolistas(10);
		System.out.println("He creado "+f2.getNumFutbolistas()+" futbolistas");
		System.exit(0);
	}
}
