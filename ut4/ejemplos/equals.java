// DA ERRORES DE COMPILACIÓN. A CORREGIR POR EL ALUMNO

class rectangulo implements Cloneable
{
	private int ancho;
	private int alto;
	private String nombre;
	
	public rectangulo(int an,int al) { ancho=an; alto=al; }
	public void incrementarAncho() { ancho++; }
	public void incrementarAlto() { alto++; }
	public void setNombre(String s) { nombre=s; }
	public int getAncho() { return ancho; }
	public int getAlto() { return alto; }
	public String getNombre() { return nombre; }
	public Object clone()
	{
        	Object objeto=null;
        	try{
            	objeto =super.clone();
        	}
        	catch(CloneNotSupportedException ex){
            System.out.println(" Error al duplicar");
        	}
	        return objeto;
	}
}

public class equals
{
	public static void main(String[] args)
	{
		rectangulo r1 = new rectangulo(5,7);
		rectangulo r2 = new rectangulo(5,7);
		rectangulo r3 = r1;
		if (r1.equals(r2))
			System.out.println("El objeto 1 y el 2 son el mismo");
		if (r1.equals(r3))
			System.out.println("El objeto 1 y el 3 son el mismo");
	}
}

