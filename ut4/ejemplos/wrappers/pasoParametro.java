public class pasoParametro
{
	// Este método incrementa el entero en una unidad
	public static void incrementa(Integer i)
	{
		// obtengo el tipo básico desde el wrapper
		int num = i.intValue();
		// incremento el tipo básico en uno
		num++;
		//System.out.println("El tipo básico dentro de la función es " + num);
		// creo el nuevo wrapper con el valor incrementado
		i=new Integer(num);
		System.out.println(i);
	}

	public static void main(String[] args)
	{
		int n=5;
		Integer i=new Integer(n);
		incrementa(i);	// 	quiero que me haga el 5++, es decir, a 6
		System.out.println(i);

		System.exit(0);
	}
}

