class A
{
	protected int num;
	public A(int n) { num=n;}
	public String toString() { return num + "\n" ; }
}

class B extends A
{
	public B(int n)
	{
		super(n);
	}
}

public class classCastException
{
	public static void main(String args[])
	{
		A a = new A(1);
		B b  = new B(5);
		// ME LO ADMITE EL COMPILADOR, UN OBJETO B TAMBIÉN ES A
		A ab = b;
		// EL COMPILADOR NO ME LO ADMITE. UN OBJETO A NO TIENE PORQUÉ SER B ... LO MOLDEO Y DESAPARECE EL ERROR DE COMPILACIÓN ...
		B ba = (B)a;
		// ... PERO EN LA EJECUCIÓN OBTENGO CLASSCASTEXCEPTION
		System.out.println(a);
		System.out.println(b);
		System.out.println(ab);
		System.out.println(ba);
	}
}
