/*
8. Define una clase jugador (con atributo nombre) que defina un método final muestraNombre, un método abstracto muestraJugador. Haz una segunda clase Futbolista que herede de la anterior con el atributo goles.
• Realiza un programa que intente instanciar algún objeto de cada una de las clases anteriores.
• Intenta redefinir o sobreescribir el método muestraNombre en la clase Futbolista. ¿Es posible?
¿Porqué?
• Crea un segundo objeto Futbolista (f2 por ejemplo, suponiendo que el primero fuera f1), que sea
final. Haz que la referencia de este segundo objeto apunte al primero (f2=f1). ¿Qué sucede?
¿Porqué?
*/

abstract class Jugador
{
    protected String nombre;

    public Jugador(){ nombre="";}

    public Jugador(String nombre)
    {
        this.nombre=nombre;
    }

    public final String muestraNombre()
    {
        return nombre;
    }

    public abstract String muestraJugador();
}

class Futbolista extends Jugador
{
    protected int goles;

    public Futbolista(){}

    public Futbolista(String nombre, int goles)
    {
        super(nombre);
        this.goles=goles;
    }
    public String muestraJugador()
    {
        return nombre;
    }

    public String muestraNombre() // error, el metodo se declaró en base como metodo final, por lo tanto no se puede sobreescribir 
    {
        return nombre;
    }   
}

public class ej8
{
    public static void main(String args[])
    {
        Jugador j1 = new Jugador("Jugador1"); // error , no es puede instanciar objetos en un clase abstracta
        Futbolista f1 = new Futbolista("Messi",100);
        final Futbolista f2 = new Futbolista("Ramos",100);
        f2 = f1; // error , un objeto declarado como final, no se puede copiar porque no puede apuntar a otra direccion que no sea la suya propia

    }
}
