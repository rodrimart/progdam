/* Primer programa de ejemplo de la modularidad en Java.

	Programa que pida 2 valores numéricos y obtenga su promedio */
	
public class ejModular1
{
	// DEFINICIÓN del método con sus PARAMETROS FORMALES y TIPO
	public static double promedio(double n1, double n2)
	{
		return ( n1 + n2)/2;
	}
	
	public static void main(String[] args)
	{
		double num1,num2,result;
		
		System.out.println("Introduce 2 números:");
		num1 = Double.parseDouble(System.console().readLine());
		num2 = Double.parseDouble(System.console().readLine());
		// LLAMADA a la función (incluyendo PARÁMETROS ACTUALES)
		// Cuidado!: en la definición se indican los tipos pero en la llamada NO
		result = promedio(num1,num2);
		System.out.println("Su promedio es " + result);
		
		System.exit(0);
	}
}
