//23. Algoritmo que permite calcular el factorial de un numero natural. SOLUCION RECURSIVA

import java.util.Scanner;

public class ejRecursividad
{
	public static int factorial(int n)
	{
		int fact=1;
		/*for (int i=2 ; i <= n ; i++)
			fact = fact*i;*/
		if (n > 1)
			return n*factorial(n-1);
		else
			return 1;
	}
	
	public static void main(String[] args) {
		int num;
		
		Scanner tec = new Scanner(System.in);
		System.out.println("Introduce un entero positivo mayor que cero:");
		num = tec.nextInt();
		
		System.out.println("Su factorial es " + factorial(num));
		
		
		System.exit(0);
	}
}
