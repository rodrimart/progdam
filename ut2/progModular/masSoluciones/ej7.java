//7. Realizar un programa que calcule los números primos entre 1 y 1000, utilizando una función que indique si un número recibido como parámetro es o no primo.

public class ej7
{
	public static boolean esPrimo(int n)
	{
		int tope = n/2;
       for (int i = 2; i <= tope; i++)
           if ( n % i==0)	// si la división es exacta
           	return false;
       return true;
	}
	
	public static void main(String[] args) {
		for ( int i=1 ; i < 1000 ; i++)	// i es cada uno de los naturales a probar
			if (esPrimo(i))	// si es primo
				System.out.println(i);
			
		System.exit(0);
	}
}
