//23. Algoritmo que permite calcular el factorial de un numero natural. SOLUCION MODULAR

import java.util.Scanner;

public class ej23
{
	public static int factorial(int n)
	{
		int fact=1;
		for (int i=2 ; i <= n ; i++)
			fact = fact*i;
		return fact;
	}
	
	public static void main(String[] args) {
		int num;
		
		Scanner tec = new Scanner(System.in);
		System.out.println("Introduce un entero positivo mayor que cero:");
		num = tec.nextInt();
		
		System.out.println("Su factorial es " + factorial(num));
		
		
		System.exit(0);
	}
}
