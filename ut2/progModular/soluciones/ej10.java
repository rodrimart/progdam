// 10*. Programa MODULAR que muestra las 10 tablas de multiplicar (para 1,2,3,...,10).

public class ej10
{
	public static void main(String[] args) {
		// Este bucle imprimirá en cada pasada una tabla de multiplicar
		for ( int num = 1 ; num < 11 ; num++)
			tablaMultiplicar(num);
			
		System.exit(0);
	}
	
	public static void tablaMultiplicar(int n)
	{
		for(int i = 1; i < 11 ; i++)
			System.out.println(n + " x " + i + " = " + i*n);
	}		
}
