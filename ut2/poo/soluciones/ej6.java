class Futbolista{
	private String nombre;
	private int goles;

	public Futbolista(){ nombre="Messi"; }
	public Futbolista(String n){ nombre=n; }
	public Futbolista(String n, int g){ nombre=n; goles=g; }

	public void setNombre(String n){ nombre=n; }
	public void setGoles(int g){ goles=g; }

	public String getNombre(){ return nombre; }
	public int getGoles(){ return goles; }

	public void anotaGol(){ goles++; }
	public String muestraFutbolista(){ return "Nombre: " + nombre + "\nGoles: " + goles; }
	public String pichichi(Futbolista f1, Futbolista f2, Futbolista f3){
		if(f1.goles > f2.goles)
		{
			if(f1.goles > f3.goles)
			{
				if(f2.goles > f3.goles){ return "* Nº1: " + f1.nombre + "\tGoles:" + f1.goles + "\n* Nº2: " + f2.nombre + "\tGoles:" + f2.goles + "\n* Nº3: " + f3.nombre + "\tGoles:" + f3.goles; } 
					else { return "* Nº1: " + f1.nombre + "\tGoles:" + f1.goles  + "\n* Nº2: " + f3.nombre + "\tGoles:" + f3.goles  + "\n* Nº3: " + f2.nombre + "\tGoles:" + f2.goles; }
			} else {
			 	return "* Nº1: " + f3.nombre + "\tGoles:" + f3.goles  + "\n* Nº2: " + f1.nombre + "\tGoles:" + f1.goles  + "\n* Nº3: " + f2.nombre + "\tGoles:" + f2.goles;
			}
		} else if (f2.goles > f3.goles)
		{
			if (f1.goles > f3.goles){ return "* Nº1: " + f2.nombre + "\tGoles:" + f2.goles  + "\n* Nº2: " + f1.nombre + "\tGoles:" + f1.goles  + "\n* Nº3: " + f3.nombre + "\tGoles:" + f3.goles; }
				else { return "* Nº1: " + f2.nombre + "\tGoles:" + f2.goles  + "\n* Nº2: " + f3.nombre + "\tGoles:" + f3.goles  + "\n* Nº3: " + f1.nombre + "\tGoles:" + f1.goles; }
		} else { return "* Nº1: " + f3.nombre + "\tGoles:" + f3.goles  + "\n* Nº2: " + f2.nombre + "\tGoles:" + f2.goles  + "\n* Nº3: " + f1.nombre + "\tGoles:" + f1.goles; }
	}

}
public class ej6{
	public static void main(String[] args) {
		Futbolista f1 = new Futbolista();
		Futbolista f2 = new Futbolista("Cristiano");
		Futbolista f3 = new Futbolista("Neymar");

		for ( int i = 0; i < 50; i++) {
			byte gol = (byte)(3 * Math.random() + 1);
			switch(gol)
			{
				case 1: f1.anotaGol();	break;
				case 2: f2.anotaGol();	break;
				case 3: f3.anotaGol(); 
			}
		}
		System.out.println(f1.pichichi(f1,f2,f3));
	}
}
